# Copyright 1996 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for SafeLogon
#
# ***********************************
# ***    C h a n g e   L i s t    ***
# ***********************************
# Date           Name       Description
# ----           ----       -----------
# 27th Oct 1994  JC         Created.
#

VPATH = @ <Support$Dir>

#
# Generic options:
#
MKDIR   = cdir
CC      = cc -ISupport: -IOS: -fach -wp
CP      = copy
CPFLAGS = ~cfr~v
OBJASM  = ObjAsm ${THROWBACK} -depend !Depend -stamp -quit
CMHG    = CMHG
LINK    = Link

#
# Libraries:
#
OSLIB  = OSLib:OSLib.o

#
# Program specific options:
#
COMPONENT = SafeLogon
TARGET    = aof.SafeLogon

OBJS = o.main

ASMS = s.main

#
# Generic rules {used by top-level build}:
#
export:
	@echo ${COMPONENT}: export complete

clean:
	-Destroy SafeLogon ${OBJS} ${OBJSD}
	@echo ${COMPONENT}: cleaned

#
# ROM target (re-linked at ROM Image build time)
#
${TARGET}: ${OBJS} ${HFILES} ${OSLIB} ${RSTUBS}
	${LINK} -o $@ -aof ${OBJS} ${OSLIB} ${RSTUBS}

# Extra {development} rules:
#

all: SafeLogon
	@Echo Made all

compile: ${ASMS}

release: SafeLogon
	%Copy SafeLogon Boot:Library.SafeLogon ~CFLNRV
	@Echo Made release

#Internal targets
SafeLogon: ${OBJS} ${OSLIB} CLib:o.Stubs
	${LINK} -output SafeLogon ${OBJS} ${OSLIB} CLib:o.Stubs
	Access SafeLogon WR/R

#General rules
.SUFFIXES: .cmhg .c .debug .o .s

.c.o:
	${CC} ${THROWBACK} -depend !Depend -c -ff $<

.c.debug:
	${CC} ${THROWBACK} -depend !Depend -c -DTRACE=1 -o $@ $<

.cmhg.o:
	${CMHG} $< $@

.c.s:
	${CC} ${THROWBACK} -depend !Depend -s -ff $<

# Dynamic dependencies:
